{-# LANGUAGE RankNTypes #-}

import XMonad
import XMonad.Actions.UpdatePointer
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers (doCenterFloat)
import XMonad.Hooks.SetWMName
import XMonad.Util.EZConfig (additionalKeys, removeKeys)
import XMonad.Util.Run(spawnPipe)
import qualified XMonad.StackSet as W
import Graphics.X11.ExtraTypes

import Data.Monoid (Endo)
import System.IO
import qualified Data.List as L

ttyWS = "1tty"
mailWS = "2mail"
emacsWS = "3emacs"
webWS = "4web"

myModMask = mod3Mask

main :: IO ()
main = do
   xmproc <- spawnPipe "xmobar"
   xmonad . docks $ def
       { workspaces = [ttyWS,mailWS,emacsWS,webWS,"5","6","7","8","9","0","-","="]
       , terminal = "xfce4-terminal"
       , modMask = myModMask
       , manageHook = manageDocks
         <+> manageHook def
         <+> myManageHooks
         <+> manageHook def
       , layoutHook = avoidStruts $ Tall 1 (2/100) (1/2) ||| Full ||| Mirror (Tall 1 (2/100) (1/2))
       , logHook = dynamicLogWithPP xmobarPP
                        { ppOutput = hPutStrLn xmproc
                        , ppTitle = xmobarColor "green" "" . shorten 50
                        }
                        >> setWMName "LG3D"
                        >> updatePointer (0.5, 0.5) (1, 1)
       } `additionalKeys`
       [((m .|. myModMask, k), do
            cfg <- fmap config ask
            windows $ f $ workspaces cfg !! i)
       | (i, k) <- [(9, xK_0), (10, xK_minus), (11, xK_equal)]
       , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
       `additionalKeys`
       [((m, k), do
            cfg <- fmap config ask
            windows $ f $ workspaces cfg !! i)
       | (i, k) <- [(0, xK_exclamdown), (1, xK_trademark), (2, xK_sterling)
                   , (3, xK_cent), (4, xK_infinity), (5, xK_section)
                   , (6, xK_paragraph), (7, xK_enfilledcircbullet)
                   , (8, xK_ordfeminine), (9, xK_masculine), (10, xK_endash)
                   , (11, xK_notequal)]
       , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
       `additionalKeys`
       [((controlMask, xK_Escape), windows W.focusDown)] -- %! Move focus to the next window
       `removeKeys`
       -- remove xinerama keys to help emacs keybindings
       [(myModMask, n) | n <- [xK_w, xK_e, xK_r, xK_p]]

    -- Default binding (example)
    -- --
    -- -- mod-[1..9], Switch to workspace N
    -- -- mod-shift-[1..9], Move client to workspace N
    -- --
    -- [((m .|. modm, k), windows $ f i)
    --     | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
    --     , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]


myManageHooks :: Query (Endo WindowSet)
myManageHooks = composeAll . concat $
   -- Most common window properties:
   -- WM_CLASS(STRING) = "chromium", "Chromium" => resource, className
   -- WM_NAME(STRING) = "Authentication" => title
   -- Placement of windows
   [ [ (className =? "Xfce4-terminal"   <&&> _machine =? "vnc-xmonad")       --> doShift ttyWS ]
   , [ (className =? "Emacs"            <&&> _machine =? "vnc-xmonad")       --> doShift emacsWS ]
   , [ (className =? "Firefox"          <&&> resource =? "Navigator")        --> doShift webWS ]
   , [ (className =? "Chromium-browser" <&&> resource =? "Chromium-browser") --> doShift webWS ]
   , [ (className =? "qutebrowser"      <&&> resource =? "__main__.py")      --> doShift webWS ]

   -- Floating windows
   , [ (className =? "Chromium"    <&&> title =? "Authentication") --> doCenterFloat ]
   , [ (className =? "XLoad"       <&&> resource =? "xload")       --> doCenterFloat ]
   , [ className =? "rdesktop"                                     --> doCenterFloat ]
   , [ (className =? "Vlc"         <&&> resource =? "vlc")         --> doCenterFloat ]
   , [ (className =? "KeePass2"    <&&> resource =? "keepass2")    --> doCenterFloat ]

   -- Using list comprehensions and partial matches
   , [ className =?  c --> doCenterFloat | c <- myFloatsC ]
   , [ fmap ( c `L.isInfixOf`) className --> doCenterFloat | c <- floatsByClass ]
   , [ fmap ( c `L.isInfixOf`) title     --> doCenterFloat | c <- floatsByTitle ]
   ]
  -- Buddy List
  where myFloatsC = ["Keepassx"]
        floatsByClass = ["Pidgin", "XOsview"] -- , "Calendar"]
        floatsByTitle = [] -- "Reminder", "Alarm"]

_role :: Query String
_role = stringProperty "WM_WINDOW_ROLE"

_machine :: Query String
_machine = stringProperty "WM_CLIENT_MACHINE"

_doMy :: ManageHook
_doMy = doCenterFloat >> ask >>= doF . _moveHere

_moveHere :: (Ord a, Eq s, Eq i, Eq a) => a -> W.StackSet i l a s sd -> W.StackSet i l a s sd
_moveHere w ws = W.shiftWin (W.currentTag ws) w ws
